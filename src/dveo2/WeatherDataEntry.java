package dveo2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Jonathan & Remco
 */
public class WeatherDataEntry {
        //Datum
        private Calendar _date;
        //Het uur van de dag
        private int _hour;
        //Windrichting (in graden) gemiddeld over de laatste 10 minuten van het afgelopen uur (360=noord, 90=oost, 180=zuid, 270=west, 0=windstil 990=veranderlijk)
        private int _windDirection;
        //Uurgemiddelde windsnelheid (in 0.1 m/s)
        private int _windSpeed;
        //Windsnelheid (in 0.1 m/s) gemiddeld over de laatste 10 minuten van het afgelopen uur
        private int _windSpeedLastTenMinutes;
        //Hoogste windstoot (in 0.1 m/s) over het afgelopen uurvak
        private int _maxWindBlast;
        //Temperatuur (in 0.1 graden Celsius) op 1.50 m hoogte tijdens de waarneming
        private double _temperature;
        //Minimumtemperatuur (in 0.1 graden Celsius) op 10 cm hoogte in de afgelopen 6 uur
        private int _minTemperature;
        //Dauwpuntstemperatuur (in 0.1 graden Celsius) op 1.50 m hoogte tijdens de waarneming
        private int _dewPointTemperature;
        //Duur van de zonneschijn (in 0.1 uren) per uurvak, berekend uit globale straling  (-1 for <0.05 uur)
        private int _durationSunshine;
        //Globale straling (in J/cm2) per uurvak
        private int _globalRadiation;
        //Duur van de neerslag (in 0.1 uur) per uurvak
        private int _durationRainfall;
        //Uursom van de neerslag (in 0.1 mm) (-1 voor <0.05 mm)
        private int _sumRainfall;
        //Luchtdruk (in 0.1 hPa) herleid naar zeeniveau, tijdens de waarneming
        private int _airPressure;
        //Horizontaal zicht tijdens de waarneming (0=minder dan 100m, 1=100-200m, 2=200-300m,..., 49=4900-5000m, 50=5-6km, 56=6-7km, 57=7-8km, ..., 79=29-30km, 80=30-35km, 81=35-40km,..., 89=meer dan 70km)
        private int _horizontalSight;
        //Bewolking (bedekkingsgraad van de bovenlucht in achtsten), tijdens de waarneming (9=bovenlucht onzichtbaar)
        private int _overcast;
        //Relatieve vochtigheid (in procenten) op 1.50 m hoogte tijdens de waarneming
        private int _relativeHumidity;
        //Weercode (00-99), visueel(WW) of automatisch(WaWa) waargenomen, voor het actuele weer of het weer in het afgelopen uur.
        private int _weatherCode;
        //Weercode indicator voor de wijze van waarnemen op een bemand of automatisch station (1=bemand gebruikmakend van code uit visuele waarnemingen, 2,3=bemand en weggelaten (geen belangrijk weersverschijnsel, geen gegevens), 4=automatisch en opgenomen (gebruikmakend van code uit visuele waarnemingen), 5,6=automatisch en weggelaten (geen belangrijk weersverschijnsel, geen gegevens), 7=automatisch gebruikmakend van code uit automatische waarnemingen)
        private int _weatherCodeIndicator;
        //Mist
        private Occurrence _fog;
        //Regen
        private Occurrence _rain;
        //Sneeuw
        private Occurrence _snow;
        //Onweer
        private Occurrence _thunder;
        //Ijzel
        private Occurrence _icing;
		// Data format uit bestand, voor omzetten naar Calendar
        private SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        /**
         * 
         * @param data
         */
        public WeatherDataEntry(String[] data) {
            try {
                if (false == data[1].isEmpty()) {
                    // Datum int omzetten naar Calender object.
                    _date = Calendar.getInstance();
                    this.df.setLenient(false); // Belangrijk anders wordt de datum niet gelezen.
                    Date date = this.df.parse(data[1].trim().toString());
                    _date.setTime(date);
                }
                if (false == data[2].isEmpty())
                    _hour = Integer.parseInt(data[2].trim());
                if (false == data[3].isEmpty())
                    _windDirection = Integer.parseInt(data[3].trim());
                if (false == data[4].isEmpty())
                    _windSpeed = Integer.parseInt(data[4].trim());
                if (false == data[5].isEmpty())
                    _windSpeedLastTenMinutes = Integer.parseInt(data[5].trim());
                if (false == data[6].isEmpty())
                    _maxWindBlast = Integer.parseInt(data[6].trim());
                if (false == data[7].isEmpty())
                    _temperature = Integer.parseInt(data[7].trim());
                if (false == data[8].isEmpty())
                    _minTemperature = Integer.parseInt(data[8].trim());
                if (false == data[9].isEmpty())
                    _dewPointTemperature = Integer.parseInt(data[9].trim());
                if (false == data[10].isEmpty())
                    _durationSunshine = Integer.parseInt(data[10].trim());
                if (false == data[11].isEmpty())
                    _globalRadiation = Integer.parseInt(data[11].trim());
                if (false == data[12].isEmpty())
                    _durationRainfall = Integer.parseInt(data[12].trim());
                if (false == data[13].isEmpty())
                    _sumRainfall = Integer.parseInt(data[13].trim());
                if (false == data[14].isEmpty())
                    _airPressure = Integer.parseInt(data[14].trim());
                if (false == data[15].isEmpty())
                    _horizontalSight = Integer.parseInt(data[15].trim());
                if (false == data[16].isEmpty())
                    _overcast = Integer.parseInt(data[16].trim());
                if (false == data[17].isEmpty())
                    _relativeHumidity = Integer.parseInt(data[17].trim());
                if (false == data[18].isEmpty())
                    _weatherCode = Integer.parseInt(data[18].trim());
                if (false == data[19].isEmpty())
                    _weatherCodeIndicator = Integer.parseInt(data[19].trim());
                if (data.length > 20)
                    if (false == data[20].isEmpty())
                        _fog = Occurrence.fromInt(Integer.parseInt(data[20].trim()));
                if (data.length > 21)
                    if (false == data[21].isEmpty())
                        _rain = Occurrence.fromInt(Integer.parseInt(data[21].trim()));
                if (data.length > 22)
                    if (false == data[22].isEmpty())
                        _snow = Occurrence.fromInt(Integer.parseInt(data[22].trim()));
                if (data.length > 23)
                    if (false == data[23].isEmpty())
                        _thunder = Occurrence.fromInt(Integer.parseInt(data[23].trim()));
                if (data.length > 24)
                    if (false == data[24].isEmpty())
                        _icing = Occurrence.fromInt(Integer.parseInt(data[24].trim()));

            } catch (ParseException e) {
                int x = 6;
            } catch (ArrayIndexOutOfBoundsException ex) {
                throw ex;
            }
        }

        public Calendar get_date() {
            return _date;
        }

        public int get_hour() {
            return _hour;
        }

        public int get_windDirection() {
            return _windDirection;
        }

        public int get_windSpeed() {
            return _windSpeed;
        }

        public int get_windSpeedLastTenMinutes() {
            return _windSpeedLastTenMinutes;
        }

        public int get_maxWindBlast() {
            return _maxWindBlast;
        }
		/**
		 * 
		 * @return temperatuur als double
		 */
        public double get_temperature() {
            return _temperature / 10;
        }

        public int get_minTemperature() {
            return _minTemperature;
        }

        public int get_dewPointTemperature() {
            return _dewPointTemperature;
        }

        public int get_durationSunshine() {
            return _durationSunshine;
        }

        public int get_globalRadiation() {
            return _globalRadiation;
        }

        public int get_durationRainfall() {
            return _durationRainfall;
        }

        public int get_sumRainfall() {
            return _sumRainfall;
        }

        public int get_airPressure() {
            return _airPressure;
        }

        public int get_horizontalSight() {
            return _horizontalSight;
        }

        public int get_overcast() {
            return _overcast;
        }

        public int get_relativeHumidity() {
            return _relativeHumidity;
        }

        public int get_weatherCode() {
            return _weatherCode;
        }

        public int get_weatherCodeIndicator() {
            return _weatherCodeIndicator;
        }

        public Occurrence get_fog() {
            return _fog;
        }

        public Occurrence get_rain() {
            return _rain;
        }

        public Occurrence get_snow() {
            return _snow;
        }

        public Occurrence get_thunder() {
            return _thunder;
        }

        public Occurrence get_icing() {
            return _icing;
        }
}