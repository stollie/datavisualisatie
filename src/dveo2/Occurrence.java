package dveo2;
/**
 *
 * @author Jonathan
 */
public enum Occurrence {
    NoOccurrence(0),
    OccuredDuringObservation(1);

    private int numVal;

    Occurrence(int numVal) {
        this.numVal = numVal;
    }

    public int getNumVal() {
        return numVal;
    }

    public static Occurrence fromInt(int input) {
        if (input == 1)
            return OccuredDuringObservation;
        return NoOccurrence;
    }
}
