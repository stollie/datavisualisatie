package dveo2;

import processing.core.*;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Jonathan & Remco
 */
public class WeatherDataKNMI {

        private WeatherDataEntry[] _weatherData;
        private PApplet papplet;

        private static List[] quaters =   new List[] {
                        Arrays.asList(Calendar.JANUARY, Calendar.FEBRUARY, Calendar.MARCH),
                        Arrays.asList(Calendar.APRIL, Calendar.MAY, Calendar.JUNE),
                        Arrays.asList(Calendar.JULY, Calendar.AUGUST, Calendar.SEPTEMBER),
                        Arrays.asList(Calendar.OCTOBER, Calendar.NOVEMBER, Calendar.DECEMBER)};
        /**
        * Get data from file
        * @param applet
        * @param dataUrl
        */
        public WeatherDataKNMI (PApplet applet, String dataUrl) {
            this.papplet = applet;
			
            //Import the data en split it to an array strings
            String[] dataStrings = this.papplet.loadStrings(dataUrl);

            //Where in the file starts the actual data?
            int startIndexData = 0;
            
           // System.out.println(dataStrings.length);
            for (int i = 0; i < dataStrings.length; i++) {
                if (dataStrings[i].startsWith("#")) {
                    startIndexData = i + 2;
                    break;
                }
            }
            
            _weatherData = new WeatherDataEntry[(dataStrings.length - startIndexData)];

            int counter = 0;
            for (int i = startIndexData; i < dataStrings.length; i++) {
                _weatherData[counter] = new WeatherDataEntry(dataStrings[i].replace(" ", "").split(","));
                counter++;
            }

        }

        public WeatherDataEntry[] getAllWeaterData() {
            return this._weatherData;
        }
        /**
         * Data ophalen van een specifiek jaar
         * @param year
         * @return array met jaar data
         */
        public ArrayList<WeatherDataEntry> getYearData(int year) {
            //System.out.println(year);
            ArrayList<WeatherDataEntry> tempData = new ArrayList<>();
            for (WeatherDataEntry row : this._weatherData) {
                //System.out.println(row.get_date().get(Calendar.YEAR));
                if (row.get_date().get(Calendar.YEAR) == year) {
                    tempData.add(row);
                }
            }
           // System.out.println("Jaar size " +tempData.size());
            return tempData;
        }
        /**
         *  Dag gemiddeldes ophalen voor het gekozen jaar en kwartaal.
         * @param year
         * @param quater
         * @param type T, P, FF, SQ of U
         * @return array van dagen. max, min en gemiddelde van de dag
         */
        public ArrayList<DayTempAverages> getDayAveragesData (int year, int quater, String type) {
            ArrayList<WeatherDataEntry> tempYearData = new ArrayList<>();
            // Eerst specifiek jaar ophalen.
            tempYearData = getYearData(year);

            // ArrayList met data van de dagen
            ArrayList<DayTempAverages> daysData = new ArrayList<>();

            // Alle temperaturen uitlezen.
            ArrayList<Double> waardes = new ArrayList<>();
            quater = quater - 1;
            //System.out.println("Get Days from: " + this.quaters[quater].toString());
            for (WeatherDataEntry hour : tempYearData) {
                // zit de huidige uur regel in de maand die we zoeken?
                if (this.quaters[quater].contains(hour.get_date().get(Calendar.MONTH))) {
                    switch (type) {
                        case "T":
                            waardes.add(hour.get_temperature());
                            break;
                        case "P":
                            waardes.add((double)hour.get_airPressure());
                            break;
                        case "FF":
                            waardes.add((double)hour.get_windSpeed());
                            break;
                        case "SQ":
                            waardes.add((double)hour.get_durationSunshine());
                            break;
                        case "U":
                            waardes.add((double)hour.get_relativeHumidity());
                            break;
                    }
                    // was de laast toegevoegde regel om 24, dan data van dag opslaan
                    if (hour.get_hour() == 24) {
                        daysData.add(new DayTempAverages(waardes));
                        // reset list voor volgende dag.
                        waardes = new ArrayList<>();
                    }
                }
            }
            return daysData;
        }
        /**
         * Alle uur data van een kwartaal ophalen, voor een specifieke waarde
         * @param year
         * @param quater
         * @param type T, P, FF, SQ of U
         * @return
         */
        public ArrayList<Double> getQuaterData (int year, int quater, String type) {
            ArrayList<WeatherDataEntry> tempYearData = new ArrayList<>();
            // Eerst specifiek jaar ophalen.
            tempYearData = getYearData(year);

            // ArrayList met data van de dagen
            ArrayList<Double> hourData = new ArrayList<>();
            
            for (WeatherDataEntry hour : tempYearData) {
                if (this.quaters[quater].contains(hour.get_date().get(Calendar.MONTH))) {
                    switch (type) {
                        case "T":
                            hourData.add(hour.get_temperature());
                            break;
                        case "P":
                            hourData.add((double)hour.get_airPressure());
                            break;
                        case "FF":
                            hourData.add((double)hour.get_windSpeed());
                            break;
                        case "SQ":
                            hourData.add((double)hour.get_durationSunshine());
                            break;
                        case "U":
                            hourData.add((double)hour.get_relativeHumidity());
                            break;
                    }
                }
            }
            return hourData;
        }
}
