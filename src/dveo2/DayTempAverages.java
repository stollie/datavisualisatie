package dveo2;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Remco
 */
public class DayTempAverages {
	ArrayList<Double> dayData;
	/**
	 *
	 * @param data alle temperatuur metingen op een dag
	 */
	public DayTempAverages (ArrayList<Double> data) {
            this.dayData = data;
	}
	/**
	 *
	 * @return min temperatuur als Integer
	 */
	public Double getMin() {
            return Collections.min(this.dayData);
	}
	/**
	 *
	 * @return max temperatuur als Integer
	 */
	public Double getMax() {
            return Collections.max(this.dayData);
	}
	/**
	 *
	 * @return gemiddelde Temperatuur als Integer
	 */
	public Double getMean() {
            double avg = 0.0;

            for(Double temp:dayData) {
                avg += temp;
            }
            return (avg/(dayData.size()));
	}
}
