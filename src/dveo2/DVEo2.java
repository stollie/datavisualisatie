package dveo2;

import processing.core.*;
import java.util.ArrayList;
import controlP5.*;
import dveo2.DayTempAverages;
import dveo2.Graph;
import dveo2.WeatherDataKNMI;

/**
 *  Opdracht 1
 *  LETOP PATH NAAR TXT AANPASSEN !
 *
 * @author Remco
 */
public class DVEo2 extends PApplet {
    ControlP5 cp5;
    DropdownList d1, d2;

    int cnt = 0;
    
    WeatherDataKNMI KNMIdata;
    ArrayList<DayTempAverages> data;
    Graph graph;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        PApplet.main(new String[]{dveo2.DVEo2.class.getName()});
    }
    
    public void setup () {
        frameRate(60);
        size(800,500);
        //noLoop();
        // init data
        this.KNMIdata = new WeatherDataKNMI(this, "D:\\SkyDrive\\Documenten\\NetbeansProjects\\DVEo2\\src\\dveo2\\data\\uurgeg_344_2001-2010.txt");
 
        // Grafiek bouwen
        this.graph = new Graph(this, 460, 400, "line");
        //this.get_data(jaar,kwartaal); // werkt nu door de dropdown al.
        
        cp5 = new ControlP5(this);
        // create a DropdownList jaren
        d1 = cp5.addDropdownList("Jaar")
                .setPosition(525, 100);
        for (int i=2000; i<2011 ;i++) {
          d1.addItem("Jaar "+i, i);
        }
        d1.setIndex(0);
        //d1.setValue(jaar);

        // create a DropdownList kwartaal
        d2 = cp5.addDropdownList("Kwartaal")
                .setPosition(625, 100);
        for (int i=1; i<5 ;i++) {
          d2.addItem("Kwartaal "+i, i);
        }
        d2.setIndex(0);
        //d2.setValue(kwartaal);
    }
    /**
     *
     * @param year
     * @param quater
     */
    public void get_data(int year, int quater) {
        this.data = KNMIdata.getDayAveragesData(year, quater, "T");
        ArrayList<Double> maxTemps = new ArrayList<>();
        ArrayList<Double> minTemps = new ArrayList<>();
        ArrayList<Double> meanTemps = new ArrayList<>();

        for (DayTempAverages day : this.data) {
            meanTemps.add(day.getMean());
            maxTemps.add(day.getMax());
            minTemps.add(day.getMin());
        }
		// Aan graph toevoegen
        this.graph.update_date(year, quater);
        this.graph.addSeries(maxTemps, "red");
        this.graph.addSeries(minTemps, "blue");
        this.graph.addSeries(meanTemps, "green");
    }

    public void draw() {
          background(255);
          this.graph.draw();
    }

    public void controlEvent(ControlEvent theEvent) {
        // DropdownList is of type ControlGroup.
        // A controlEvent will be triggered from inside the ControlGroup class.
        // therefore you need to check the originator of the Event with
        // if (theEvent.isGroup())
        // to avoid an error message thrown by controlP5.
        if (theEvent.isGroup()) {
            //System.out.println("event from group : "+theEvent.getGroup().getValue()+" from "+theEvent.getGroup());
            int jaar = (int)d1.getValue();
            int kwartaal = (int)d2.getValue();
            //System.out.println("Select data : "+jaar+" en "+kwartaal);
            // Update grafiek Axis
            this.graph.update_date(jaar,kwartaal);
            // data ophalen
            this.get_data(jaar,kwartaal);
            // check if the Event was triggered from a ControlGroup
        }
        else if (theEvent.isController()) {
            //System.out.println("event from controller : "+theEvent.getController().getValue()+" from "+theEvent.getController());
        }
    }
}
