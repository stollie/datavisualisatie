package dveo2;

import processing.core.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;

/**
 *
 * @author Remco
 */
public class Graph {
    PApplet papplet;
    // Hoe groot de graph mag worden
    int height;
    int width;

    int padding = 50;

    String graphType = "line";

    // Het de linker hoek van de grafiek
    int xPosBase;
    int yPosBase;

    ArrayList<ArrayList> series = new ArrayList<>();
    ArrayList<String> colors = new ArrayList<>();

    double yMaxVal = 30.0;
    double yMinVal = 0.0;
    double xMaxVal = 30.0;
    double xMinVal = 0.0;

    int yAxisItems = 30; // stappen tussen ymin en ymax
    int ySteps; // pixels per stap
    int yValueSteps = 5;
    int xAxisItems = 30;
    int xSteps; // pixels per stap
    int xValueSteps = 5;

    // Maanden van elk kwartaal
    String[][] maanden =   new String[][] {
                    new String[] {"Januari", "Februari", "Maart"},
                    new String[] {"April", "Mei", "Juni"},
                    new String[] {"Juli", "Augustus", "September"},
                    new String[] {"Oktober", "November", "December"}};
    // Current
    int year;
    int quarter;

    /**
     * Init van de grafiek
     * @param applet
     * @param width
     * @param height
     * @param type type grafiek
     */
    public Graph(PApplet applet, int width, int height, String type) {
        this.papplet	= applet;
        this.graphType  = type;
        this.height     = height;
        this.width      = width;
        // begin positie, we willen links onder als basis.
        this.xPosBase = this.padding;
        this.yPosBase = this.papplet.height - this.padding;
    }
    /**
     *
     */
    public void draw() {
        // line / point dikte
        this.papplet.strokeWeight(1);
        // zwarte lijnen
        this.papplet.stroke(0);
        // Basis lijnen x en y maken.
        this.papplet.line(this.xPosBase, this.yPosBase, this.xPosBase + this.width, this.yPosBase); // x
        this.papplet.line(this.xPosBase, this.yPosBase, this.xPosBase, this.yPosBase - this.height); // y

        if (this.series.size() > 0) {
            // Aparte functies op dit moment.
            if (this.graphType == "line")
                this.drawxAxisMonths();
            if (this.graphType == "plot")
                this.drawxAxis();

            this.drawyAxis();
            // in de bovenstaande functies worden de xSteps en xValueStep berekend, zodat ze hieronder gebruikt worden.
            if (this.graphType == "line")
                this.drawLines();
            if (this.graphType == "plot")
                this.drawPlot();
        }
    }
    /**
     *
     * @param list
     * @param kleur
     */
    public void addSeries (ArrayList<Double> list, String kleur) {
        this.series.add(list);
        this.colors.add(kleur);
    }
    /**
     * Lijnen van de grafiek tekenen.
     */
    public void drawLines() {
        int i = 0;
        for (ArrayList<Double> serie : this.series) {
            if (this.colors.get(i) == "red")
                this.papplet.stroke(255,0,0);
            if (this.colors.get(i) == "green")
                this.papplet.stroke(0,255,0);
            if (
                this.colors.get(i) == "blue" ||
                this.colors.get(i) == ""
            ) {
                this.papplet.stroke(0,0,255);
            }
            i++;

            int day = 0;
            int prevY = this.yPosBase;
            int prevX = this.xPosBase;

            int curY;
            int curX = this.xPosBase;
            //int lijn = 0;
            for (Double tempature : serie) {
                day++;
                curY = this.calcyPos(tempature);
                curX = (int)prevX + (int)this.xSteps;
                if (day == 1) {
                    curX -= (int)this.xSteps;
                }
                //System.out.println("Dag " + day +" - "+tempature + " xPos " + curX  +" yPos " + curY);
                if(day > 1) {
                    //lijn++;
                    //System.out.println("Lijntje " + lijn + " =  van X " + prevX + " Y " + prevY +" naar X " + curX + " Y " + curY);
                    this.papplet.line(prevX, prevY, curX, curY);
                }
                // Prev Opslaan
                prevY = curY;
                prevX = curX;
            }
        }
    }
    /**
     * Points tekenen.
     */
    public void drawPlot () {
        if (
            this.series.size() == 2 &&
            this.series.get(0).size() == this.series.get(1).size()
        ) {
            this.papplet.strokeWeight(2);
            for (int i = 0; i < this.series.get(0).size(); i++) {
                this.papplet.point(this.calcxPos((double)this.series.get(0).get(i)), this.calcyPos((double)this.series.get(1).get(i)));
            }
        }
    }
    /*
    * xAxis voor maanden
    */
    public void drawxAxisMonths () {
        // Tekst settings
        this.papplet.fill(0, 102, 153);
        this.papplet.textAlign(this.papplet.CENTER);
        // positie van de line op de laatste dag van de maand.
        int xcur = this.xPosBase - this.xSteps; // -1 stap omdat dag 1 op de xposbase is
        this.papplet.stroke(10, 10, 10);
        // Maanden
        int namesteps = (this.width / 3);
        int xNamecur = namesteps;
        int month = 0;
        int totalDays = 0;
        int tmpQuarter = this.quarter;
        // array start bij 0
        tmpQuarter = tmpQuarter - 1;
        // begin maand berekeken voor elke kwartaal
        if (tmpQuarter >= 1)
            month = (tmpQuarter + tmpQuarter * 2);
        // loop moet max 3 keer lopen.
        for (String name : this.maanden[tmpQuarter]) {
            Calendar tempcal = new GregorianCalendar(this.year, month, 1);
            // Get the number of days in that month
            int daysInMonth = tempcal.getActualMaximum(Calendar.DAY_OF_MONTH);
            //System.out.println(daysInMonth + " " + name + " m: "+month+" q: " + this.quarter);
            xcur += (daysInMonth * this.xSteps);
            // Maand eind dag
            this.papplet.line(xcur, this.yPosBase+1, xcur, this.yPosBase - 6);
            // Maand naam
            this.papplet.text(name, xNamecur, this.yPosBase + 20);
            xNamecur += namesteps;
            month++;
            totalDays += daysInMonth;
        }
        this.xAxisItems = totalDays;
        // Bereken aantal pixels per stap op de x as
        this.xSteps = (int)(this.width / this.xAxisItems);

        // tick omlaag
        int xtest = this.xPosBase;
        for (int i = 0; i < totalDays; i++) {
            this.papplet.line(xtest, this.yPosBase, xtest, this.yPosBase - 3);
            xtest = xtest + this.xSteps;
        }
    }

    public void drawxAxis () {
        // resetten maar weer.
        this.xMaxVal = 30.0;
        this.xMinVal = 0.0;
        // Misschien alleen bij update uitvoeren
        double maxVal;
        double minVal;
        ArrayList<ArrayList> TempSeries;
        TempSeries = (ArrayList<ArrayList>)this.series.clone();
        if (this.graphType == "plot") {
            // alleen het eerste item uit de array nodig.
            TempSeries.remove(1);
        }
        boolean keeplow = false; // laagste waarde vast zetten
        for (ArrayList<Double> serie : TempSeries) {
            minVal = Collections.min(serie);
            maxVal = Collections.max(serie);
            //System.out.println("X Min " + minVal + " Max " + maxVal);
            if (maxVal > this.xMaxVal) {
                double maxVerschil = maxVal - this.xMaxVal;
                int deelbaar = (int)maxVerschil / this.xValueSteps;
                this.xMaxVal += (this.xValueSteps * (deelbaar + 1));
                //System.out.println("Max Verschil " + maxVerschil +"  D " + deelbaar +" waardes " + minVal + " en " +  maxVal+ "  NIEUW: Max temp " + this.xMaxVal + " Min temp " + this.xMinVal);
            }
            if (minVal > this.xMinVal) {
                double minVerschil = minVal - this.xMinVal;
                int deelbaar = (int)minVerschil / this.xValueSteps;
                this.xMinVal += (this.xValueSteps * (deelbaar + 1));
                //System.out.println("Min groter Verschil " + minVerschil +"  D " + deelbaar +" waardes " + minVal + " en " +  maxVal+ "  NIEUW: Max temp " + this.xMaxVal + " Min temp " + this.xMinVal);
            }
            if (minVal < this.xMinVal) {
                double minVerschil = this.xMinVal - minVal;
                int deelbaar = (int)minVerschil / this.xValueSteps;
                this.xMinVal -= (this.xValueSteps * (deelbaar + 1));
                keeplow = true;
                //System.out.println("Min lager Verschil " + minVerschil +"  D " + deelbaar +" waardes " + minVal + " en " +  maxVal+ "  NIEUW: Max temp " + this.xMaxVal + " Min temp " + this.xMinVal);
            }
        }
        if ((this.xMaxVal - this.xMinVal) > 120) {
            this.xValueSteps = 20;
        }
        this.xAxisItems = (int) (((this.xMaxVal - this.xMinVal) / this.xValueSteps)  + 1);
        //System.out.println("xAxisItems "+this.xAxisItems);
        // Bereken aantal pixels per stap op de x as
        this.xSteps = (int)(this.width / this.xAxisItems);
        //System.out.println("xSteps " + this.xSteps);

        this.papplet.textAlign(this.papplet.CENTER);
        // basis waardes
        int xWaarde = (int)this.xMinVal;
        int xcur = this.xPosBase;
        // lijn kleur
        this.papplet.stroke(0);
        for (int i = 0; i < this.xAxisItems + 1; i++) {
            //System.out.println(xcur);
            this.papplet.text(xWaarde , xcur, this.yPosBase + 20);
            this.papplet.line(xcur, this.yPosBase, xcur , this.yPosBase + 5);
            xWaarde = xWaarde + this.xValueSteps;
            xcur = xcur + this.xSteps ;
        }
    }

    /**
     * Waardes weergeven en horizontale lijnen in graph
     */
    public void drawyAxis () {
        // Resetten maar weer
        this.yMaxVal = 30.0;
        this.yMinVal = 0.0;
        // Misschien alleen bij update uitvoeren
        double maxVal;
        double minVal;

        ArrayList<ArrayList> TempSeries;
        TempSeries = (ArrayList<ArrayList>)this.series.clone();
        if (this.graphType == "plot") {
            // alleen het tweede item uit de array nodig.
            TempSeries.remove(0);
        }
        boolean keeplow = false; // laagste waarde vast zetten
        for (ArrayList<Double> serie : TempSeries) {
            minVal = Collections.min(serie);
            maxVal = Collections.max(serie);
            //System.out.println("Y Min " + minVal + " Max " + maxVal + " Verschil " + (maxVal - minVal));
            //System.out.println(maxVal+" > " +  this.yMaxVal + " " + (maxVal > this.yMaxVal));
            if (maxVal > this.yMaxVal) {
                double maxVerschil = maxVal - this.yMaxVal;
                int deelbaar = (int)maxVerschil / this.yValueSteps;
                this.yMaxVal += (this.yValueSteps * (deelbaar + 1));
                //System.out.println("Max verhogen Verschil " + maxVerschil +"  D " + deelbaar +" waardes " + minVal + " en " +  maxVal+ "  NIEUW: Max temp " + this.yMaxVal + " Min temp " + this.yMinVal);
            }
            if (minVal > this.yMinVal && keeplow == false) {
                double minVerschil =  minVal - this.yMinVal;
                int deelbaar = (int)minVerschil / this.yValueSteps;
                this.yMinVal += (this.yValueSteps * (deelbaar + 1));
                //System.out.println("Min verkleinen Verschil " + minVerschil +"  D " + deelbaar +" waardes " + minVal + " en " +  maxVal+ "  NIEUW: Max temp " + this.yMaxVal + " Min temp " + this.yMinVal);
            }
            if (minVal < this.yMinVal) {
                double minVerschil = this.yMinVal - minVal;
                int deelbaar = (int)minVerschil / this.yValueSteps;
                this.yMinVal -= (this.yValueSteps * (deelbaar + 1));
                keeplow = true;
                //System.out.println("Min verlagen Verschil " + minVerschil +"  D " + deelbaar +" waardes " + minVal + " en " +  maxVal+ "  NIEUW: Max temp " + this.yMaxVal + " Min temp " + this.yMinVal);
            }

        }
        // Zelf even een waarde gekozen om stap te vergroten.
        if ( (this.yMaxVal - this.yMinVal) > 120 ) {
            this.yValueSteps = 20;
        }
        // aantal waarde stappen (lijnen) in de grafiek
        this.yAxisItems = (int) (((this.yMaxVal - this.yMinVal) / this.yValueSteps)  + 1);

        // Tekst settings
        this.papplet.fill(0, 102, 153);
        this.papplet.textAlign(this.papplet.RIGHT);
        // basis waardes
        int temp = (int)this.yMinVal;
        int ycur = this.yPosBase;
        // lijn kleur

        // Bereken aantal pixels per stap op de y as
        this.ySteps = (int)(this.height / this.yAxisItems);

        this.papplet.stroke(0);
        if (this.graphType == "line")
            this.papplet.stroke(0, 102, 153, 51);

        for (int i = 0; i < this.yAxisItems; i++) {
            this.papplet.text(temp , this.xPosBase - 5, ycur);
            if (this.graphType == "line") {
                this.papplet.line(this.xPosBase, ycur, this.xPosBase + this.width, ycur);
            }
            else {
                this.papplet.line(this.xPosBase, ycur, this.xPosBase - 5, ycur);
            }
            temp = temp + this.yValueSteps;
            ycur = ycur - this.ySteps;
        }
    }

    /**
     * temperatuur omzetten naar positie in grafiek
     * @param tempature
     * @return yPos
     */
    private int calcyPos(double tempature) {
        double nWaarde = tempature - this.yMinVal; // normale waarde voor in de grafiek.
        //System.out.println("Y input " + tempature + " norm " + nWaarde + " Min " +  this.yMinVal + " Stapy " + this.ySteps);
        //System.out.println("PosY " +(int)(this.yPosBase - (nWaarde * (double)this.ySteps)));
        return (int)(this.yPosBase - (nWaarde * ((double)this.ySteps / this.yValueSteps)));
    }

    /**
     * temperatuur omzetten naar positie in grafiek
     * @param tempature
     * @return xPos
     */
    private int calcxPos(double tempature) {
        double nWaarde = tempature - this.xMinVal; // normale waarde voor in de grafiek.
        //System.out.println("X input " + tempature + " norm " + nWaarde + " Min"   +  this.xMinVal + " PosY " + (int)(this.xPosBase + nWaarde * this.xSteps));
        return (int)(this.xPosBase + (nWaarde * (this.xSteps / this.xValueSteps)));
    }

    /**
     * Series legen en jaar + kwartaal updaten
     * @param year
     * @param quater
     */
    public void update_date (int year, int quater) {
        this.yMaxVal = 30.0;
        this.yMinVal = 0.0;
        this.xMaxVal = 30.0;
        this.xMinVal = 0.0;
        this.yValueSteps = 5;
        this.xValueSteps = 5;
        this.series.clear();
        this.year = year;
        this.quarter = quater;
    }

}
