package dveo2;

import processing.core.*;
import java.util.ArrayList;
import controlP5.*;

/**
*  Opdracht 2
*  LETOP PATH NAAR TXT AANPASSEN !
* @author Remco
*/
public class DVEo2plot extends PApplet {
    ControlP5 cp5;
    DropdownList d1;
    DropdownList d2;
    RadioButton r1;
    RadioButton r2;
    int cnt = 0;
    
    WeatherDataKNMI KNMIdata;
    ArrayList<DayTempAverages> data;
    Graph graph;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        PApplet.main(new String[]{dveo2.DVEo2plot.class.getName()});
    }
    
    public void setup () {
        frameRate(60);
        size(800,500);
        // init
        this.KNMIdata = new WeatherDataKNMI(this, "D:\\SkyDrive\\Documenten\\NetbeansProjects\\DVEo2\\src\\dveo2\\data\\uurgeg_344_2001-2010.txt");
 
        // Grafiek bouwen
        this.graph = new Graph(this, 460, 400, "plot");
        //this.get_data(jaar,kwartaal);
        
        cp5 = new ControlP5(this);
        // create a DropdownList jaren
        d1 = cp5.addDropdownList("Jaar")
                .setPosition(525, 100);
        for (int i=2000; i<2011 ;i++) {
          d1.addItem("Jaar "+i, i);
        }
        d1.setIndex(0);

        // create a DropdownList kwartaal
        d2 = cp5.addDropdownList("Kwartaal")
                .setPosition(625, 100);
        for (int i=1; i<5 ;i++) {
          d2.addItem("Kwartaal "+i, i);
        }
        d2.setIndex(0);
        fill(0, 102, 153);
        this.r1 = cp5.addRadioButton("first",525,180)
                  .setSize(20,20)
                  .setColorForeground(color(120))
                  .setColorActive(color(200))
                  .setColorLabel(color(0))
                    .setSpacingColumn(5)
                  .addItem("Rel. vochtigheid",1)
                  .addItem("Luchtdruk",2)
                  .addItem("Windsnelheid",3)
                  .addItem("Temperatuur",4)
                  .addItem("Duur zonnenschijn",5)
                  .activate(2)
                  ;
        this.r2 = cp5.addRadioButton("second",650,180)
                  .setSize(20,20)
                  .setColorForeground(color(120))
                  .setColorActive(color(200))
                    .setSpacingColumn(5)
                  .setColorLabel(color(0))
                  .addItem("Rel. vochtigheid 2",1)
                  .addItem("Luchtdruk 2",2)
                  .addItem("Windsnelheid 2",3)
                  .addItem("Temperatuur 2",4)
                  .addItem("Duur zonnenschijn 2",5)
                  .activate(1)
                  ;
    }
    /**
     *
     * @param year
     * @param quater
     */
    public void get_data(int year, int quater, String first, String second) {
        ArrayList<Double> waardes1 = KNMIdata.getQuaterData(year, quater, first);
        ArrayList<Double> waardes2 = KNMIdata.getQuaterData(year, quater, second);

        //this.data = KNMIdata.getQuaterData(year, quater, first);
        // for (Double day : this.data) {
            //System.out.println(day.getMean());
        //    waardes1.add(day.getMean());
        //}
        //this.data = KNMIdata.getQuaterData(year, quater, second);
        //for (DayTempAverages day : this.data) {
        //    //System.out.println(day.getMean());
        //    waardes2.add(day.getMean());
        //}
        // Aan graph toevoegen
        this.graph.update_date(year, quater);
        this.graph.addSeries(waardes1, "");
        this.graph.addSeries(waardes2, "");
    }

    public void draw() {
          background(255);
          this.graph.draw();
    }

    public void controlEvent(ControlEvent theEvent) {
        // DropdownList is of type ControlGroup.
        // A controlEvent will be triggered from inside the ControlGroup class.
        // therefore you need to check the originator of the Event with
        // if (theEvent.isGroup())
        // to avoid an error message thrown by controlP5.
        String[] plotOpties = {"niets","U", "P", "FF", "T", "SQ"};

        if (theEvent.isGroup()) {
            //System.out.println("First " + plotOpties[(int)r1.getValue()] + " second " + plotOpties[(int)r2.getValue()]);
            //System.out.println("event from group : "+theEvent.getGroup().getValue()+" from "+theEvent.getGroup());
            int jaar = (int)d1.getValue();
            int kwartaal = (int)d2.getValue();
            //System.out.println("Select data : "+jaar+" en "+kwartaal);
            // Update grafiek Axis
            this.graph.update_date(jaar,kwartaal);
            // data ophalen
            this.get_data(jaar,kwartaal, plotOpties[(int)r1.getValue()], plotOpties[(int)r2.getValue()] );
        }
        else if (theEvent.isController()) {
            //System.out.println("event from controller : "+theEvent.getController().getValue()+" from "+theEvent.getController());
        }
    }

    public void radioButton(int a) {
      //System.out.println("a radio Button event: "+a);
    }
}
